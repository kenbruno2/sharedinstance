//
//  FooClass.h
//  SharedInstance
//
//  Created by Kenneth Bruno II on 2/5/16.
//  Copyright © 2016 Kenneth Bruno II. All rights reserved.
//

#import "SharedInstance-Swift.h"

#ifndef FooClass_h
#define FooClass_h

@interface FooClass (NoInit)
/*! @abstract Use sharedInstance: to obtain the singleton. */
+ (instancetype) new NS_UNAVAILABLE;
/*! @abstract Use sharedInstance: to obtain the singleton. */
- (instancetype) init NS_UNAVAILABLE;
@end

#endif /* FooClass_h */
