//
//  FooClass.swift
//  SharedInstance
//
//  Created by Kenneth Bruno II on 2/2/16.
//  Copyright © 2016 Kenneth Bruno II. All rights reserved.
//

import Foundation

@objc(FooClass)
public class FooClass: NSObject {
  // singleton
  public static let sharedInstance = FooClass()
  
  public var value: String
  private override init() {
    self.value = "asdf"
  }
}
