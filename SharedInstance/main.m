//
//  main.m
//  SharedInstance
//
//  Created by Kenneth Bruno II on 2/2/16.
//  Copyright © 2016 Kenneth Bruno II. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharedInstance-Swift.h"
#import "FooClass.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
      // insert code here...
    FooClass* foo  = [FooClass sharedInstance]; // All good here
    FooClass* foo2 = [[FooClass alloc] init];   // 'init' is unavailable
    FooClass* foo3 = [FooClass new];            // 'new' is unavailable
  }
    return 0;
}
