//
//  FooClass.swift
//  
//
//  Created by Kenneth Bruno II on 2/2/16.
//
//

import Foundation

public class FooClass: NSObject {
  // singleton
  public static let sharedInstance = FooClass()
  
  public let value: String
  
  private override init() {
    self.value = "asdf"
  }
}
